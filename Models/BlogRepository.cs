﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MicroBlog.Models
{
    public class BlogRepository
    {
        readonly BlogContext _db;

        public BlogRepository(BlogContext db)
        {
            _db = db;
        }

        // Get article by id
        public async Task<Article> GetArticle(Guid id)
        {
            return await _db.Articles.Where(a => a.Id == id).Select(a => a).FirstOrDefaultAsync();
        }

        // Get articles that can be viewed
        public async Task<List<Article>> GetArticles()
        {
            var articles = _db.Articles.Where(a => a.PublishedAt != null && a.DeletedAt == null && a.Body != null)
                .Select(a => a).OrderByDescending(a => a.PublishedAt);
            return await articles.ToListAsync();
        }

        // Delete article by setting delete tag
        public async Task DeleteArticle(Guid? id)
        {
            if (id != null)
            {
                var article = await GetArticle(id.Value);
                article.DeletedAt = DateTime.Now;
                await _db.SaveChangesAsync();
            }
        }

        // Create new article by article view model
        public async Task<Article> CreateArticle(Guid? id, string body, string email)
        {
            if (id != null)
            {
                var oldArticle = await GetArticle(id.Value);
                email = oldArticle.Email;
            }

            var article = new Article
            {
                Id = Guid.NewGuid(),
                OldId = id,
                Body = body,
                Email = email,
                PublishedAt = null // Will be set after authorization
            };

            _db.Articles.Add(article);
            await _db.SaveChangesAsync();

            return article;
        }

        // This method is called by email link to allow authorized changes
        async Task<Article> AllowChanges(Article article)
        {
            if (article != null)
            {
                //  Create or update? 
                if (article.OldId != null)
                {
                    var oldArticle = await GetArticle(article.OldId.Value);
                    article.PublishedAt = oldArticle.PublishedAt;
                    oldArticle.DeletedAt = DateTime.Now;
                }
                else
                {
                    article.PublishedAt = DateTime.Now; // Newly created article will not show
                }

                await _db.SaveChangesAsync();
            }

            return article;
        }

        // This method is called by email link to allow authorized changes
        public async Task<Article> AllowChanges(Guid id)
        {
            var article = await GetArticle(id);
            return await AllowChanges(article);
        }
    }
}