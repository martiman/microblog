﻿using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Configuration;

namespace MicroBlog.Models
{
    public class EmailService
    {
        public EmailService(IConfiguration configuration)
        {
            _config = configuration;
        }

        private IConfiguration _config { get; }

        // Send ordinary email by smtp configured by json
        private void SendEmail(string recepient, string body)
        {
            var client = new SmtpClient(_config["EmailService:Smtp"]);
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(_config["EmailService:Login"], _config["EmailService:Password"]);
            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(_config["EmailService:From"]);
            mailMessage.To.Add(recepient);
            mailMessage.Body = body;
            mailMessage.Subject = "Microblog auth";
            mailMessage.IsBodyHtml = true;
            client.Send(mailMessage);
        }

        // Send authorization email to author article
        public void SendAuthEmail(string host, Article article)
        {
            var body = $"<a href=\"http://{host}/Article?id={article.Id}&handler=allow\">Potvrdit změnu na blogu</a>";
            SendEmail(article.Email, body);
        }
    }
}