﻿using System;

namespace MicroBlog.Models
{
    public class Article
    {
        public Guid Id { get; set; }
        public Guid? OldId { get; set; }
        public string Body { get; set; }
        public string Email { get; set; }
        public DateTime? PublishedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}