# README #

Microblog je napsaný pomocí Razor Pages ASP.NET Core 2.0. Jde o jednoduchý systém pro zveřejňování článků. 
Pro reálné využití není použitelný, šlo pouze o vyzkoušení ověřování změn pomocí emailů. Žádná změna se na blogu neprojeví dokud nebude potvrzena přes email.

### Konfigurace ###

Nejdříve nastavte parametry v souboru appsettings.json a poté založte databázi pomoci migrací, nebo pomocí skriptu v adresáři Sql.

### Todo ###



