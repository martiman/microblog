using System;
using System.Threading.Tasks;
using MicroBlog.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MicroBlog.Pages
{
    public class ArticleModel : PageModel
    {
        readonly EmailService _auth;
        readonly BlogRepository _repo;

        [BindProperty]
        public Guid? Id { get; set; }

        [BindProperty]
        public string Body { get; set; }

        [BindProperty]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Email musí být vyplněn z důvodu autorizace změn")]
        public string Email { get; set; }

        public ArticleModel(BlogRepository repo, EmailService auth)
        {
            _repo = repo;
            _auth = auth;
        }

        public async Task OnGet(Guid? id)
        {
            if (id != null)
            {
                var article = await _repo.GetArticle(id.Value);
                Id = article.Id;
                Body = article.Body;
                Email = article.Email;
            }
        }

        public async Task<IActionResult> OnGetAllowAsync(Guid id)
        {
            await _repo.AllowChanges(id);

            return RedirectToPage("/Index");
        }

        public async Task<IActionResult> OnPostDeleteAsync()
        {
            Body = null; //založením nového článku s prázdným tělěm máme na mysli odstranění po potvrzení emailem
            var article = await _repo.CreateArticle(Id, Body, Email);
            var host = PageContext.HttpContext.Request.Host.ToString();
            _auth.SendAuthEmail(host, article);

            return RedirectToPage("/Index");
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (Id != null) ModelState.MarkFieldValid("Email");
            if (!ModelState.IsValid) return Page();

            var host = PageContext.HttpContext.Request.Host.ToString();
            var article = await _repo.CreateArticle(Id, Body, Email);
            _auth.SendAuthEmail(host, article);

            return RedirectToPage("/Index");
        }
    }
}