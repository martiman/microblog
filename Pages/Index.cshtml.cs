﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MicroBlog.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MicroBlog.Pages
{
    public class IndexModel : PageModel
    {
        readonly BlogRepository _repo;

        [BindProperty]
        public IEnumerable<Article> Articles { get; set; }

        public IndexModel(BlogRepository repo)
        {
            _repo = repo;
        }

        public async Task OnGet()
        {
            Articles = await _repo.GetArticles();
        }
    }
}