﻿// <auto-generated />

using System;
using MicroBlog.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MicroBlog.Migrations
{
    [DbContext(typeof(BlogContext))]
    internal class BlogContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MicroBlog.Models.Article", b =>
            {
                b.Property<Guid>("Id")
                    .ValueGeneratedOnAdd();

                b.Property<string>("Body");

                b.Property<DateTime?>("DeletedAt");

                b.Property<string>("Email");

                b.Property<Guid?>("OldId");

                b.Property<DateTime?>("PublishedAt");

                b.HasKey("Id");

                b.ToTable("Articles");
            });
#pragma warning restore 612, 618
        }
    }
}